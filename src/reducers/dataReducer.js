import { 
	TEST_ACTION,
} from '../types';


const INIT = {};

export default (state = INIT, action) => {
	switch (action.type) {
		case TEST_ACTION:
			return 100;
		
		default:
			return state;
	}
};
