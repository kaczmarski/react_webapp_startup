import React, { Fragment } from 'react';
import Navbar from '../Navbar/Navbar';

function App() {
  return (
		<Fragment>
			
			<Navbar />
			
			<div className="container">
				<div className="row">
					<div className="col my-5">
						<h1 className="display-4 text-center">
							{process.env.REACT_APP_NAME}
						</h1>
					</div>
				</div>
			</div>
			
		</Fragment>
  );
}

export default App;
