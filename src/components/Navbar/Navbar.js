import React from 'react';


const Navbar = () => {
	
	return (
		<nav className="navbar fixed-top navbar-dark bg-dark">
			<div className="container">
				<div className="navbar-brand">{process.env.REACT_APP_NAME} <span className="small text-muted">{process.env.REACT_APP_AUTHOR_NAME}</span></div>
			</div>
		</nav>
	);
	
};

export default Navbar;
