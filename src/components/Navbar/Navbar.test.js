import React from 'react';
import ReactDOM from 'react-dom';
import Navbar from './Navbar';

import { shallow, mount } from 'enzyme';


describe( 'Navbar', () => {
	
	
	it('renders without crashing', () => {
		const div = document.createElement('div');
		ReactDOM.render(<Navbar />, div);
		ReactDOM.unmountComponentAtNode(div);
	});
	
	
	it('renders without crashing', () => {
		shallow(<Navbar />);
	});
	
	
	it('renders packege AUTHOR_NAME', () => {
		const wrapper = mount(<Navbar />);
		const text = process.env.REACT_APP_AUTHOR_NAME;
		//expect(wrapper.contains(text)).toEqual(true);
		expect(wrapper).toIncludeText(text);
	});
	
	
	it('renders packege NAME', () => {
		const wrapper = mount(<Navbar />);
		const text = process.env.REACT_APP_NAME;
		//expect(wrapper.contains(text)).toEqual(true);
		expect(wrapper).toIncludeText(text);
	});
	
	
});
