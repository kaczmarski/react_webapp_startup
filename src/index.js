import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';

import combineReducers from './reducers';

import './scss/_index.scss';

import App from './components/App/App';


const middleware = [
	thunk,
];


const	composeEnhancers = (process.env.NODE_ENV === 'development') ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose : compose;


const store = createStore(
	combineReducers, 
	{data: 10},
	composeEnhancers(
    applyMiddleware(...middleware)
  )
);


ReactDOM.render(
	<Provider store={store}>
		<App />
	</Provider>
, document.getElementById('root'));
